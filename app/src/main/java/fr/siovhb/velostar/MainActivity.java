package fr.siovhb.velostar;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    ListView listViewStations;
    ArrayList<String> lesDonnees;
    ArrayAdapter<String> adaptateur;
    private EditText editTextNom;
    private Button buttonAjouter;

    /**
     * Méthode appelée lors de la création de l'activité
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_stationvelos_main);
        initialisations();
    }

    /**
     * Initialise les attributs privés référençant les widgets de l'interface utilisateur
     * Instancie les écouteurs et les affecte sur les objets souhaités
     */
    private void initialisations() {
        // instanciation de la source de données, ici 4 chaînes dans une collection de chaînes
        this.lesDonnees = new ArrayList<String>();
        this.lesDonnees.add("Champs libres");
        this.lesDonnees.add("Marboeuf");
        this.lesDonnees.add("Pain levé");
        this.lesDonnees.add("La Poterie");

        // on récupère l'id du composant ListView
        this.listViewStations = (ListView) this.findViewById(R.id.listViewStations);
        // on crée l'adaptateur en lui indiquant l'activité, le layout et la source de données
        this.adaptateur = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, this.lesDonnees);
        // on associe l'adaptateur au composant ListView
        this.listViewStations.setAdapter(adaptateur);

        // on traite le clic court sur les éléments de la liste
        this.listViewStations.setOnItemClickListener(new ItemOnClick());

        // on traite le clic long sur les éléments de la liste
        this.listViewStations.setOnItemLongClickListener(new ItemOnLongClick());

        // on récupère l'id de la zone d'édition d'un système
        this.editTextNom = (EditText) this.findViewById(R.id.editTextSysteme);

        // on récupère l'id du bouton Ajouter
        this.buttonAjouter = (Button) this.findViewById(R.id.buttonAjouter);
        this.buttonAjouter.setOnClickListener(new BoutonAjouterOnClick());

        editTextNom.addTextChangedListener(new btnAjouterVisible());
    }

    //region classes écouteurs

    /**
     * Classe interne servant d'écouteur de l'événement click sur les éléments de la liste
     */
    private class ItemOnClick implements AdapterView.OnItemClickListener {
        public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
            Toast.makeText(MainActivity.this, lesDonnees.get(position), Toast.LENGTH_LONG).show();
        }
    }
    private class ItemOnLongClick implements AdapterView.OnItemLongClickListener{
        public boolean onItemLongClick(AdapterView<?> adapterView, View view, int position, long id) {
            adaptateur.remove(lesDonnees.remove(position));
            return false;
        }
    }

    /**
     * Classe interne servant d'écouteur de l'événement click sur le bouton Ajouter
     */
    private class BoutonAjouterOnClick implements View.OnClickListener {
        public void onClick(View v) {
            // on récupère la donnée à partir d'une zone editable txtDonnee
            String laDonnee = editTextNom.getText().toString();
            lesDonnees.add(0,laDonnee); // ajout en début de liste
            // on prévient l'adaptateur que la source de données a changé
            adaptateur.notifyDataSetChanged();
        }
    }
    private class btnAjouterVisible implements TextWatcher {

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            if (editTextNom.length() > 0){
                buttonAjouter.setEnabled(true);
            }
            else{
                buttonAjouter.setEnabled(false);
            }
        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    }
    //endregion
}